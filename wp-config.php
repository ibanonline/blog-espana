<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog_espana');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('FS_METHOD', 'direct');
define('AUTH_KEY',         'N58}i@ kNmFE(QyBlD7{!q[F<gxeypGp0TUOzWx.nns,6m<# Q}; jZd{^YuCxG7');
define('SECURE_AUTH_KEY',  ')Cr~ddd?Tby|v`(Ju#nJ]:}dY6.[8=zzWLT#QSI=<6_l,iI-xky|-OUaQmAnWrLz');
define('LOGGED_IN_KEY',    '2P#S(dKG6k1UK)MB-b?H/+tR9xA;y;18<Ly(cT9IcX)|h #Nj9:TV2&fQcRht}m,');
define('NONCE_KEY',        '=bCTw+{hZ;<n)@m ]d!Bb;.e]>Dr4!Jf^{pw=%i,]/inre]Bf6CrGt<YWpHG>vgY');
define('AUTH_SALT',        '+8%e9>s1?>Mt1.BJBF![7`8%^C#r]qqeJ%;i8_t+Qu_yh=dzjUx|9B^ng4d*dF3R');
define('SECURE_AUTH_SALT', '.}]V<-pMN~g4N1@!/Rn|nQzIElO{j!ZM47)3Ryr:,UPd;,Iqnn,zij/$e DprjYG');
define('LOGGED_IN_SALT',   '`LGlDN$-fkegBxG]/lm)HU3DAM+Z-=(LLI?1dWR%ER? fL1PfWpk|oM{YIK>Y*yj');
define('NONCE_SALT',       'pWPzIn;LLFkijcb^>y5*w,z_[q~{.{R.vWX7uM/;Sc?_!}.Jv`&.G;&$]=bC]&>2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
