<?php
/**
 * The template for displaying the footer.
 *
 * Contains all content after the main content area and sidebar
 *
 * @package WorldStar
 */

?>

</div><!-- #content -->

<?php do_action( 'worldstar_before_footer' ); ?>

<div id="footer" class="footer-wrap">

<footer id="colophon" class="site-footer container clearfix" role="contentinfo">

<?php do_action( 'worldstar_footer_menu' ); ?>
  <h4>iBAN ONLINE L.T.D. <p style="font-weight: normal;">Todos los derechos reservados</p></h4>

<!-- .site-info -->

</footer><!-- #colophon -->

</div>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
